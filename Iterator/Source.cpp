#include "Iterator.h"
using std::cout;
using std::cin;
using std::endl;


void main()
{
	List<int> myList;
	myList.add(3).add(4).add(5).add(7).add(9).add(11).add(13).add(15);
	cout << "List: " << myList;
	List<int> copyList;
	copyList = myList;
	cout << "Copy list: " << copyList;
	myList.reverse();
	cout << "Reverse list: " << myList;
	cout << "Iterator: ";
	for (auto& it : myList)
		cout << it << ' ';
	cout << endl;
	try {
		auto t = myList.getAt(9); cout << "4. element: " << t.data << endl;
	}
	catch (std::exception& ex)
	{
		cout << ex.what() << endl;
	}
	List<int>::Iterator it1 = ++myList.begin(); cout << "Prefix it1: " << *it1 << endl;
	List<int>::Iterator it2 = myList.begin()++; cout << "Postfix it2: " << *it2 << endl;
	cout << "it1 == it2 ? " << std::boolalpha << (it1 == it2) << endl;
	cout << "it1 != it2 ? " << std::boolalpha << (it1 != it2) << endl;
	it1 = it2;
	cout << "it2: " << *it2 << endl;
	cout << "it1: " << *it1 << endl;
	
	cin.ignore();
	cin.get();
}