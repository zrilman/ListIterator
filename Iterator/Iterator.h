#pragma once
#include <iostream>
#include <algorithm>
#include <stdexcept>

template<class T>
class List
{
	struct Node
	{
		T data;
		Node* next;
		Node(const T& idata) : data(idata), next(nullptr) {}
	};
public:
	class Iterator : public std::iterator<std::forward_iterator_tag, Node*>
	{
		friend class List;
	public:
		Iterator& operator= (const Iterator&);
		Iterator& operator++ ();
		Iterator operator++ (int);
		T& operator* () noexcept;
		bool operator== (const Iterator&) noexcept;
		bool operator!= (const Iterator&) noexcept;
		Iterator& operator+= (const int n);
	private:
		Node* itr;
		explicit Iterator(Node*) noexcept;
	};

	List() noexcept;
	List(const List&) noexcept;
	List(List&&) noexcept;
	~List() noexcept;
	List& operator= (const List&);
	List& operator= (List&&);
	List& add(const T&);
	void reverse();
	Node& getAt(const int) noexcept(false);
	Iterator begin() noexcept;
	Iterator end() noexcept;
private:
	Node* head;
	int n = 0;
	void copy(const List&);
	void move(List&&);
	template<class T>
	friend std::ostream& operator<< (std::ostream&, const List<T>&) noexcept;
};

template<class T>
List<T>::List() noexcept: head(nullptr), n(0) {}
template<class T>
List<T>::List(const List& other) noexcept
{
	copy(other);
}

template<class T>
List<T>::List(List&& other) noexcept
{
	move(std::move(other));
}

template<class T>
List<T>::~List() noexcept
{
	Node* temp = head;
	while (temp)
	{
		Node* toDel = temp;
		temp = temp->next;
		delete toDel;
	}
	head = nullptr;
	n = 0;
}

template<class T>
List<T>& List<T>::operator=(const List& other)
{
	if (this != &other)
		copy(other);
	return *this;
}

template<class T>
List<T>& List<T>::operator= (List&& other)
{
	if (list != &other)
		move(std::move(other));
	return *this;
}

//Listovi se dodaju na pocetak
template<class T>
List<T>& List<T>::add(const T& data)
{
	Node* toAdd = new Node(data);
	if (head)
	{
		toAdd->next = head;
		head = toAdd;
		++n;
	}
	else
		head = toAdd;
	return *this;
}

template<class T>
void List<T>::copy(const List& other)
{
	List<T>::~List();
	n = other.n;
	for (Node* temp = other.head; temp; temp = temp->next)
		add(temp->data);
	reverse();
}

template<class T>
void  List<T>::move(List&& other)
{
	n = std::move(other.n);
	head = std::move(other.head);
	other.head = nullptr;
}

template<class T>
typename List<T>::Node& List<T>::getAt(const int index) noexcept(false)
{
	if (n < index - 1 || index <= 0)
		throw std::out_of_range("Index out of bounds");
	auto it = begin();
	for (int i = 0; i < n && i < index; ++i, ++it);
	return *it.itr;
}

template<class T>
typename List<T>::Iterator& List<T>::Iterator::operator= (typename const List<T>::Iterator& other)
{
	itr->data = other.itr->data;
	
	return *this;
}

template<class T>
typename List<T>::Iterator& List<T>::Iterator::operator+= (const int n)
{
	for (int i = 0; i < n; ++i)
		itr = itr->next;
	return Iterator(itr);
}
template<class T>
typename List<T>::Iterator List<T>::end() noexcept
{
	return Iterator(nullptr);
}

template<class T>
typename List<T>::Iterator List<T>::begin() noexcept
{
	return Iterator(head);
}

template<class T>
List<T>::Iterator::Iterator(typename List<T>::Node* add) noexcept: itr(add) {}

template<class T>
typename List<T>::Iterator& List<T>::Iterator::operator++ ()
{
	itr = itr->next;
	return *this;
}

template<class T>
typename List<T>::Iterator List<T>::Iterator::operator++ (int)
{
	Node* temp = itr;
	itr = itr->next;
	return Iterator(temp);
}

template<class T>
typename T& List<T>::Iterator::operator* () noexcept
{
	return itr->data;
}

template<class T>
bool List<T>::Iterator::operator== (typename const List<T>::Iterator& other) noexcept
{
	return itr == other.itr;
}

template<class T>
bool List<T>::Iterator::operator!= (typename const List<T>::Iterator& other) noexcept
{
	return itr != other.itr;
}

template<class T>
void List<T>::reverse()
{
	Node *prev = nullptr;
	Node *next;
	while (head)
	{
		next = head->next;
		head->next = prev;
		prev = head;
		head = next;
	}
	head = prev;
}

template<class T>
std::ostream& operator<< (std::ostream& os, const List<T>& list) noexcept
{
	for (typename List<T>::Node* temp = list.head; temp; temp = temp->next)
		os << temp->data << ' ';
	return os << std::endl;
}